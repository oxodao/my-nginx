FROM nginx:alpine

RUN mkdir -p /etc/nginx/snippets /etc/nginx/sites-enabled /etc/nginx/certs /var/www

COPY ./nginx.conf /etc/nginx/nginx.conf
COPY ./ssl.conf /etc/nginx/snippets/ssl.conf
COPY ./example.html /var/www/index.html

CMD [ "nginx" ]
